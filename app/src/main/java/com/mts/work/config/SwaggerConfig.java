package com.mts.work.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Collections;

/**
 * @author Developed by Magora Team (magora-systems.com). 2016.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {


    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Bean
    public Docket publicApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("public-api")
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .paths(PathSelectors.any())
                .build()
                .directModelSubstitute(LocalDate.class, String.class)
                .directModelSubstitute(LocalTime.class, String.class)
                .directModelSubstitute(LocalDateTime.class, String.class)
                .apiInfo(apiInfo());
    }


    private ApiInfo apiInfo() {
        return new ApiInfo(
                "MTS test work",
                "API for MTS test work",
                "1.0.0",
                "[TBD] ToS",
                new Contact("Alex", "http://", "miknix@ngs.ru"),
                "[TBD] API License",
                "TBD-API-License-URL",
                Collections.emptyList());
    }
}