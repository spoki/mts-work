package com.mts.work.service;

import com.mts.work.domain.Currency;
import com.mts.work.domain.cursor.CursorData;
import com.mts.work.domain.cursor.QueryCursor;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */
public interface CurrencyService {

    CursorData<Currency> getCurrencies(QueryCursor queryCursor);

}
