CREATE USER currency_db_user WITH PASSWORD 'xxxxxx';

CREATE DATABASE currency_db
ENCODING = 'UTF8'
LC_COLLATE = 'en_US.utf8'
LC_CTYPE = 'en_US.utf8'
CONNECTION LIMIT = -1;

GRANT ALL privileges ON DATABASE currency_db TO currency_db_user;