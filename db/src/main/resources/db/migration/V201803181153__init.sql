CREATE TABLE currencies(
  code VARCHAR(3) PRIMARY KEY,
  name TEXT NOT NULL CHECK (char_length(name) <= 255),
  update_time TIMESTAMPTZ NOT NULL,
  tsv TSVECTOR
);

INSERT INTO currencies VALUES ('USD', 'United States Dollar', now());
INSERT INTO currencies VALUES ('SGD', 'Singapore Dollar', now());
INSERT INTO currencies VALUES ('BDT', 'Bangladeshi Taka', now());
INSERT INTO currencies VALUES ('CHF', 'Swiss Franc', now());
INSERT INTO currencies VALUES ('DZD', 'Algerian Dinar', now());
INSERT INTO currencies VALUES ('GHS', 'Ghanaian Cedi', now());
INSERT INTO currencies VALUES ('INR', 'Indian Rupee', now());
INSERT INTO currencies VALUES ('KRW', 'South Korean Won', now());
INSERT INTO currencies VALUES ('MAD', 'Moroccan Dirham', now());
INSERT INTO currencies VALUES ('NZD', 'New Zealand Dollar', now());
INSERT INTO currencies VALUES ('PLN', 'Polish Zloty', now());
INSERT INTO currencies VALUES ('SEK', 'Swedish Krona', now());
INSERT INTO currencies VALUES ('TRY', 'Turkish Lira', now());
INSERT INTO currencies VALUES ('AED', 'United Arab Emirates Dirham', now());
INSERT INTO currencies VALUES ('BHD', 'Bahraini Dinar', now());
INSERT INTO currencies VALUES ('CLP', 'Chilean Peso', now());
INSERT INTO currencies VALUES ('EGP', 'Egyptian Pound', now());
INSERT INTO currencies VALUES ('HKD', 'Hong Kong Dollar', now());
INSERT INTO currencies VALUES ('IRR', 'Iranian Rial', now());
INSERT INTO currencies VALUES ('KWD', 'Kuwaiti Dinar', now());
INSERT INTO currencies VALUES ('MXN', 'Mexican Peso', now());
INSERT INTO currencies VALUES ('OMR', 'Omani Rial', now());
INSERT INTO currencies VALUES ('QAR', 'Qatari Rial', now());
INSERT INTO currencies VALUES ('TWD', 'New Taiwan Dollar', now());
INSERT INTO currencies VALUES ('ARS', 'Argentine Peso', now());
INSERT INTO currencies VALUES ('BRL', 'Brazilian Real', now());
INSERT INTO currencies VALUES ('CNY', 'Chinese Yuan', now());
INSERT INTO currencies VALUES ('EUR', 'Euro', now());
INSERT INTO currencies VALUES ('IDR', 'Indonesian Rupiah', now());
INSERT INTO currencies VALUES ('JOD', 'Jordanian Dinar', now());
INSERT INTO currencies VALUES ('LBP', 'Lebanese Pound', now());
INSERT INTO currencies VALUES ('MYR', 'Malaysian Ringgit', now());
INSERT INTO currencies VALUES ('PHP', 'Philippine Peso', now());
INSERT INTO currencies VALUES ('RUB', 'Russian Ruble', now());
INSERT INTO currencies VALUES ('THB', 'Thai Baht', now());
INSERT INTO currencies VALUES ('VND', 'Vietnamese Dong', now());
INSERT INTO currencies VALUES ('AUD', 'Australian Dollar', now());
INSERT INTO currencies VALUES ('CAD', 'Canadian Dollar', now());
INSERT INTO currencies VALUES ('COP', 'Colombian Peso', now());
INSERT INTO currencies VALUES ('GBP', 'British Pound Sterling', now());
INSERT INTO currencies VALUES ('ILS', 'Israeli New Sheqel', now());
INSERT INTO currencies VALUES ('JPY', 'Japanese Yen', now());
INSERT INTO currencies VALUES ('LKR', 'Sri Lankan Rupee', now());
INSERT INTO currencies VALUES ('NGN', 'Nigerian Naira', now());
INSERT INTO currencies VALUES ('PKR', 'Pakistani Rupee', now());
INSERT INTO currencies VALUES ('SAR', 'Saudi Riyal', now());
INSERT INTO currencies VALUES ('TND', 'Tunisian Dinar', now());
INSERT INTO currencies VALUES ('ZAR', 'South African Rand', now());

UPDATE currencies SET tsv = to_tsvector('pg_catalog.simple', name);