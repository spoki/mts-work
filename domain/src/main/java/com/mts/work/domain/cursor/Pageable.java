package com.mts.work.domain.cursor;

/**
 * Created by kemenov on 26.04.2017.
 */
public interface Pageable {

    String PAGE_SIZE_PARAMETER = "pageSize";
    String PAGE_PARAMETER = "page";

    int getPageSize();

    int getPage();


}
