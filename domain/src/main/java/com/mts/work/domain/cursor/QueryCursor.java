package com.mts.work.domain.cursor;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */

@Getter
@Setter
public class QueryCursor extends BaseCursor{

    @Length(min = 1)
    private String query;
}
