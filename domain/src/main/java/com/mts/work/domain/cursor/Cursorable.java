package com.mts.work.domain.cursor;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */

public interface Cursorable {

    String PAGE_SIZE_PARAMETER = "pageSize";
    String CURSOR_PARAMETER = "cursor";

    int getPageSize();

    String getCursor();

}
