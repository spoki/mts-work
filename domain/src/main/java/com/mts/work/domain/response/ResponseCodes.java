package com.mts.work.domain.response;

/**
 * Class declares common error codes
 *
 * @author Fyodor Kemenov
 *         Developed by Magora Team (magora-systems.com). 2015.
 */
public final class ResponseCodes {

    private ResponseCodes() {
    }

    public static final String SUCCESS_CODE = "success";

}
