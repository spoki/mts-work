package com.mts.work.domain.cursor;


/**
 * Created by kemenov on 08.10.2016.
 */
public class PageableImpl implements Pageable {

    private final int page;
    private final int pageSize;

    public PageableImpl(int page, int pageSize) {
        this.page = page;
        this.pageSize = pageSize;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public int getPageSize() {
        return pageSize;
    }
}
