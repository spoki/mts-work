package com.mts.work.domain.cursor;

import lombok.Data;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */

@Data
public class BaseCursor implements Cursorable {

//    @ApiModelProperty(value = "Cursor for pagination")
    protected String cursor;

//    @ApiModelProperty(value = "Size of page")
    protected int pageSize = 20;
}
