package com.mts.work.domain.cursor;


import java.util.List;

/**
 * Created by kemenov on 07.10.2016.
 */
public class Cursors {

    private final static int DEFAULT_SIZE = 100;
    private final static int FIRST_PAGE_INDEX = 0;

    public final static Pageable FIRST_PAGE = new PageableImpl(FIRST_PAGE_INDEX, DEFAULT_SIZE);


    public static Pageable buildPageRequest(Cursorable cursorable) {
        String cursor = cursorable.getCursor();
        if (cursor != null && !cursor.isEmpty()) {
            return new PageableImpl(
                    Integer.parseInt(cursor),
                    cursorable.getPageSize()
            );
        }
        if (cursorable.getPageSize() == DEFAULT_SIZE) {
            return FIRST_PAGE;
        }
        return new PageableImpl(0, cursorable.getPageSize());
    }

    public static <T> CursorData<T> buildPageCursor(List<T> items, Pageable request) {
        return new CursorData<T>(
                items,
                items.isEmpty() ? null : String.valueOf(request.getPage() + 1),
                request.getPage() == 0 ?
                        null :
                        String.valueOf(request.getPage() - 1)
        );
    }

}
