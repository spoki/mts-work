package com.mts.work.domain.cursor;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */

public class CursorData<T> extends CollectionData<T> {

    private final String nextCursor;
    private final String prevCursor;

    @JsonCreator
    public CursorData(@JsonProperty("items") List<T> items,
                      @JsonProperty("nextCursor") String nextCursor,
                      @JsonProperty("prevCursor") String prevCursor) {
        super(items);
        this.nextCursor = nextCursor;
        this.prevCursor = prevCursor;
    }

    public String getNextCursor() {
        return nextCursor;
    }

    public String getPrevCursor() {
        return prevCursor;
    }
}
