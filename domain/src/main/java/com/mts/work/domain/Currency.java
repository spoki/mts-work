package com.mts.work.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Currency {

    private String code;

    private String name;

}
