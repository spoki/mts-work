package com.mts.work.domain.cursor;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */

public class CollectionData<T> {

    private final List<T> items;

    @JsonCreator
    public CollectionData(@JsonProperty("items") List<T> items) {
        this.items = items;
    }

    public List<T> getItems() {
        return items;
    }
}
