package com.mts.work.service.repository;

import com.mts.work.service.entity.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */
public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Long>, CurrencyRepositoryCustom {
}
