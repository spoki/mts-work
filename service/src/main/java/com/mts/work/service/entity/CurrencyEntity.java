package com.mts.work.service.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */

@Getter
@Setter
@Table(name = "currencies")
@Entity(name = "currencies")
public class CurrencyEntity {

    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "tsv", columnDefinition = "tsvector", updatable = false, insertable = false)
    private String tsv;
}
