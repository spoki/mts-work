package com.mts.work.service.repository;

import com.mts.work.service.entity.CurrencyEntity;
import com.mts.work.service.entity.CurrencyEntity_;
import com.mts.work.service.orm.FTSCustomDialect;
import com.mts.work.service.orm.TextSearchHelper;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2017.
 */
public class CurrencyRepositoryImpl implements CurrencyRepositoryCustom {

    @PersistenceContext
    private EntityManager em;

    private final TextSearchHelper textSearchHelper = new TextSearchHelper();

    @Override
    public List<CurrencyEntity> findByName(String searchText, int page, int pageSize) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<CurrencyEntity> criteriaQuery = cb.createQuery(CurrencyEntity.class);
        Root<CurrencyEntity> root = criteriaQuery.from(CurrencyEntity.class);

        if (searchText != null) {
            criteriaQuery.where(
                    cb.isTrue(cb.function(FTSCustomDialect.FTS, Boolean.class, root.get(CurrencyEntity_.tsv), cb.literal(textSearchHelper.buildQuery(searchText))))
            );
        }

        criteriaQuery.orderBy(cb.asc(root.get(CurrencyEntity_.name)));
        TypedQuery<CurrencyEntity> query = em.createQuery(criteriaQuery);

        query.setFirstResult(page * pageSize);
        query.setMaxResults(pageSize);

        return query.getResultList();
    }
}
