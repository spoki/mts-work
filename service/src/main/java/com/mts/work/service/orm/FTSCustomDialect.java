package com.mts.work.service.orm;

import org.hibernate.dialect.PostgreSQL95Dialect;

/**
 * Created by kemenov on 20.10.2016.
 */
public class FTSCustomDialect extends PostgreSQL95Dialect {

    public static final String FTS = "fts";

    public FTSCustomDialect() {
        registerFunction("fts", new PostgreSQLFullTextSearchFunction());
    }
}