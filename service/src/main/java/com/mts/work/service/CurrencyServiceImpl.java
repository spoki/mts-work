package com.mts.work.service;

import com.mts.work.domain.Currency;
import com.mts.work.domain.cursor.CursorData;
import com.mts.work.domain.cursor.Cursors;
import com.mts.work.domain.cursor.Pageable;
import com.mts.work.domain.cursor.QueryCursor;
import com.mts.work.service.repository.CurrencyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */
@Service
@RequiredArgsConstructor
public class CurrencyServiceImpl implements CurrencyService {

    private final CurrencyRepository currencyRepository;

    @Transactional(readOnly = true)
    public CursorData<Currency> getCurrencies(QueryCursor queryCursor) {
        Pageable pageable = Cursors.buildPageRequest(queryCursor);
        List<Currency> collect = currencyRepository.findByName(queryCursor.getQuery(), pageable.getPage(), pageable.getPageSize())
                .stream()
                .map(c -> new Currency(c.getCode(), c.getName()))
                .collect(Collectors.toList());

        return Cursors.buildPageCursor(collect, pageable);
    }
}
