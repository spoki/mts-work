package com.mts.work.service.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */

@Getter
@Setter
@MappedSuperclass
public class PersistentLong {

    public static final String GENERIC_GENERATOR_NAME = "optimized-sequence";

    @Id
    @Column(name = "ID", nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERIC_GENERATOR_NAME)
    @Access(AccessType.PROPERTY)
    private Long id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersistentLong that = (PersistentLong) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : super.hashCode();
    }
}
