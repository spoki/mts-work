package com.mts.work.service.repository;

import com.mts.work.service.entity.CurrencyEntity;

import java.util.List;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2017.
 */
public interface CurrencyRepositoryCustom {

    List<CurrencyEntity> findByName(String searchText, int page, int pageSize);
}
