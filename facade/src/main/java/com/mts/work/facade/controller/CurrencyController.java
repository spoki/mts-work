package com.mts.work.facade.controller;

import com.mts.work.domain.Currency;
import com.mts.work.domain.cursor.CursorData;
import com.mts.work.domain.cursor.QueryCursor;
import com.mts.work.domain.response.SuccessResponse;
import com.mts.work.service.CurrencyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tokar;
 * Developed by Magora Team (magora-systems.com). 2018.
 */
@RestController
@RequestMapping(value = "/api/${api-version}/currencies")
@RequiredArgsConstructor
public class CurrencyController {

    private final CurrencyService currencyService;

    @RequestMapping(method = RequestMethod.GET)
    public SuccessResponse<CursorData<Currency>> getCurrencies(QueryCursor queryCursor) {
        return new SuccessResponse<>(currencyService.getCurrencies(queryCursor));
    }
}
